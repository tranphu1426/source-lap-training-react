import styled from "styled-components";

export type TodoModel = {
  id: number;
  title: string;
};

type TodoListProps = {
  todos?: TodoModel[];
  onTodoClick?: (todo: TodoModel) => void;
};

const TodoList = (props: TodoListProps) => {
  const { todos, onTodoClick } = props;

  function handleClick(todo: TodoModel) {
    if (onTodoClick) {
      onTodoClick(todo);
    }
  }

  return (
    <TodoListStyled>
      <ul className="todo-list">
        {todos?.map((todo) => (
          <li key={todo.id} onClick={() => handleClick(todo)}>
            {todo.title}
          </li>
        ))}
      </ul>
    </TodoListStyled>
  );
};

const TodoListStyled = styled.div``;

TodoList.defaultProps = {
  todos: [],
};

export default TodoList;
