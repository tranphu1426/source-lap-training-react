import { useState } from "react";

type TodoFormProps = {
  onSubmit?: (formvalue: FormValues) => void;
};

export type FormValues = {
  title: string;
};

const TodoForm = (props: TodoFormProps) => {
  const { onSubmit } = props;
  const [value, setValue] = useState("");

  function hanleValueChange(e: React.ChangeEvent<HTMLInputElement>) {
    console.log(e.target.value);
    setValue(e.target.value);
  }

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    // prevent reloading browser
    event.preventDefault();

    if (!onSubmit) return;

    const formValues: FormValues = {
      title: value,
    };

    onSubmit(formValues);

    // reset form
    setValue("");
  }

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" value={value} onChange={hanleValueChange} />
    </form>
  );
};

export default TodoForm;
