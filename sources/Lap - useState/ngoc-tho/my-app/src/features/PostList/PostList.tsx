import styled from "styled-components";

export type PostModel = {
  id: number;
  title: string;
};

type PostListProps = {
  posts: PostModel[];
};

export const PostList = (props: PostListProps) => {
  const { posts } = props;

  return (
    <PostListStyled>
      <ul className="post-list">
        {posts.map((post) => (
          <li key={post.id}>{post.title}</li>
        ))}
      </ul>
    </PostListStyled>
  );
};

const PostListStyled = styled.div``;
