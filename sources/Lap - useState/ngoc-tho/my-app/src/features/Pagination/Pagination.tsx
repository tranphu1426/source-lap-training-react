export type PaginationModel = {
  _page: number;
  _limit: number;
  _totalRows: number;
};

type PaginationProps = {
  pagiantion: PaginationModel;
  onPageChange?: Function;
};

export const Pagination = (props: PaginationProps) => {
  const { pagiantion, onPageChange } = props;
  const { _page, _limit, _totalRows } = pagiantion;
  const totalPages = Math.ceil(_totalRows / _limit);

  function handlePageChange(newPage: number) {
    if (onPageChange) {
      onPageChange(newPage);
    }
  }

  return (
    <div>
      <button disabled={_page <= 1} onClick={() => handlePageChange(_page - 1)}>
        Prev
      </button>
      <button
        disabled={_page >= totalPages}
        onClick={() => handlePageChange(_page + 1)}
      >
        Next
      </button>
    </div>
  );
};
